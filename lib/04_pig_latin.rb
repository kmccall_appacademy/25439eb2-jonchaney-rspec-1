require 'byebug'

def translate(sentence)
  sentence.split.map do |word|
    latinize(word)
  end.join(" ")
end

def latinize(word)
  debugger
  until is_vowel?(word[0].downcase)
    if is_q?(word[0])
      word = word[2..-1] + word[0..1]
    else
      word = word[1..-1] + word[0]
    end
  end
  ay(word)
end

def is_vowel?(letter)
  vowels = "aeiou"
  vowels.include?(letter)
end

def is_q?(letter)
  return true if letter == 'q' else false
end

def ay(word)
  word + "ay"
end
