def echo(word)
  word
end

def shout(word)
  word.upcase
end

def repeat(word,num=2)
  words = []
  i = 0
  while i < num
    words << word
    i+=1
  end
  words.join(" ")
end

def start_of_word(str,num)
  str[0...num]
end

def first_word(str)
  str.split[0]
end

def titleize(str)
  str.split.map.with_index do |word, idx|
    if !little_word?(word) || idx == 0
      word.capitalize
    else
      word
    end
  end.join(" ")
end

def little_word?(word)
  little_words = ["over","and","the"]
  little_words.include?(word)
end
