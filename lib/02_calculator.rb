def add (num1,num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  if arr.length == 0
    return 0
  end
  arr.reduce(:+)
end

def multiply(arr)
  arr.reduce(:*)
end

def power(num1, num2)
    num1**num2
end

def factorial(num)
  (0..num).reduce(:+)
end
